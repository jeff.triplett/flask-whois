import pythonwhois

from flask import Flask, jsonify, request
from urlparse import urlparse


app = Flask(__name__)


@app.route('/')
def whois_app():
    domain = request.args.get('domain', None)
    format = request.args.get('format', 'raw')
    if domain:
        domain = urlparse(domain).netloc or urlparse(domain).path
        data = pythonwhois.net.get_whois_raw(domain)
        if format == 'json':
            parsed = pythonwhois.parse.parse_raw_whois(data, normalized=True)
            if 'raw' in parsed:
                del(parsed['raw'])
            return jsonify(parsed)
        elif format == 'raw':
            return '<pre>{0}</pre>'.format(data[0])
        else:
            return data[0]

    return 'No ?domain= was specified! &format=[raw|json] is optional.'


if __name__ == '__main__':
    whois_app.run(debug=True)
